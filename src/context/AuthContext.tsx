import React, { Dispatch, SetStateAction } from "react";

export interface IAuthContext {
  isLoggedIn: boolean;
  webId?: string;
  setIsLoggedIn?: Dispatch<SetStateAction<boolean>>;
}

export const AuthContext = React.createContext<IAuthContext>({
  isLoggedIn: false,
});
