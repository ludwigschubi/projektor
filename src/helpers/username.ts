export const getUsernameFromWebId = (webId: string) => {
  let username = "";
  if (webId.startsWith("https://id.inrupt.com"))
    username = new URL(webId as string).pathname.split("/").pop() as string;
  return username;
};
