import {
  getDefaultSession,
  handleIncomingRedirect,
} from "@inrupt/solid-client-authn-browser";
import { useEffect, useState } from "react";
import { AuthContext } from "./context/AuthContext";

interface Props {
  children: JSX.Element;
}

function AuthWrapper(props: Props) {
  const { children } = props;
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [loadingAuth, setLoadingAuth] = useState(true);
  const [activeWebId, setActiveWebId] = useState<string | undefined>();

  useEffect(() => {
    if (!isLoggedIn && loadingAuth) {
      if (!window.location.pathname.startsWith("/login")) {
        localStorage.setItem(
          "lastRoute",
          window.location.pathname + window.location.search
        );
      }
      handleIncomingRedirect({ restorePreviousSession: true }).then(
        async () => {
          const session = getDefaultSession();
          if (session?.info.isLoggedIn) {
            setLoadingAuth(false);
            setIsLoggedIn(true);
            setActiveWebId(session.info.webId);
          } else {
            setTimeout(() => {
              setLoadingAuth(false);
            }, 2000);
          }
        }
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingAuth]);

  if (loadingAuth) {
    return (
      <div
        style={{
          height: "fit-content",
          width: "fit-content",
          margin: "auto",
          marginTop: "20%",
        }}
      >
        <h2>Starting Projektor...</h2>
      </div>
    );
  }

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn,
        setIsLoggedIn,
        webId: activeWebId,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export default AuthWrapper;
