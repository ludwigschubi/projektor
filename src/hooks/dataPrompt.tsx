import { useState } from "react";

export const useDataPrompts = (storage: string) => {
  const [progress, setProgress] = useState(0);
  const dataPrompts = findDataPrompts(storage);
  return {
    progress,
    dataPrompts,
    runDataPrompts: () => {
      setProgress(10);
      checkExistingData(dataPrompts);
      setProgress(50);
      handleMissingData(dataPrompts);
      setProgress(100);
    },
  };
};

const findDataPrompts = (storage: string) => {
  console.debug(storage);
};

const checkExistingData = (dataPrompts: any) => {
  console.debug(dataPrompts);
};

const handleMissingData = (dataPrompts: any) => {
  console.debug(dataPrompts);
};
