import { login } from "@inrupt/solid-client-authn-browser"
import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router"
import { Formik } from "formik"
import { toast } from "react-toastify"

import Button from "../../components/Button/Button"
import { AuthContext } from "../../context/AuthContext"
import logoArt from "../../logo-art.png"
import Page from "../Page"
import FormInput from "../../components/FormInput/FormInput"
import Modal from "../../components/Modal/Modal"
import styles from "./LoginPage.module.scss"

function LoginPage() {
  const { isLoggedIn } = useContext(AuthContext)
  const navigate = useNavigate()
  const [customLoginVisible, setCustomLoginVisible] = useState(false)

  useEffect(() => {
    if (isLoggedIn) {
      const lastRoute = localStorage.getItem("lastRoute")
      if (lastRoute && !lastRoute.startsWith("/login")) {
        navigate(lastRoute)
      } else {
        navigate("/")
      }
    }
  }, [isLoggedIn, navigate])

  const loginWithProvider = (oidcIssuer: string) => {
    localStorage.removeItem("discoveredData")
    login({
      oidcIssuer, // Solid Identity Provider
      redirectUrl:
        process.env.NODE_ENV === "development"
          ? undefined
          : `https://projektor.technology/login`,
      clientId:
        process.env.NODE_ENV === "development"
          ? undefined
          : "https://projektor.technology/projektor.jsonld", // Client Identifier
    }).catch((e) => {
      toast("Please enter a valid Identity Provider URL")
    })
  }

  return (
    <Page title="The Projektor">
      <div className={styles.main}>
        <img src={logoArt} className={styles.art} alt="Projektor Pixel Art" />
        <section>
          <h1 className={styles.header}>Welcome to Projektor!</h1>
          <p>
            Projektor is a prototype app that allows you to share and collect
            media that you like, with and from the people in your network.
          </p>
          {/* <h3 className={styles.header}>
            At the moment Projektor only supports Inrupt's ESS.
          </h3> */}
        </section>
        <Button
          color="purple"
          onClick={() => {
            loginWithProvider("https://login.inrupt.com")
          }}
        >
          Inrupt Pod Login
        </Button>
        {/* <Button
          color="grey"
          onClick={() => {
            setCustomLoginVisible(true);
          }}
        >
          Custom Pod Login
        </Button> */}
        <Modal
          visible={customLoginVisible}
          onCancel={() => setCustomLoginVisible(false)}
        >
          <Formik
            initialValues={{ idp: "" }}
            onSubmit={(values) => loginWithProvider(values.idp)}
          >
            {({ handleSubmit, dirty }) => (
              <div className={styles.form}>
                <FormInput
                  special
                  type="text"
                  name="idp"
                  label="e.g. https://login.inrupt.com"
                />
                <Button
                  color="green"
                  disabled={!dirty}
                  onClick={() => handleSubmit()}
                >
                  Login
                </Button>
              </div>
            )}
          </Formik>
        </Modal>
      </div>
    </Page>
  )
}

export default LoginPage
